#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CDIR=$( pwd )
cd $DIR/../themes
rm -f ../zips/fpa-comites-populares.zip
zip -r ../zips/fpa-comites-populares.zip fpa-comites-populares -x "fpa-comites-populares/node_modules/*"
