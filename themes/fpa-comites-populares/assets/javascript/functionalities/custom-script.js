document.addEventListener('DOMContentLoaded', function() {
    // Call function here
    syncInputWithElement('.fieldset-cf7mls .titulo-peticao', '.fieldset-cf7mls .show-titulo-peticao')
    syncInputWithElement('.fieldset-cf7mls .estado1', '.fieldset-cf7mls .show-estado1')
    syncInputWithElement('.fieldset-cf7mls .apresentacao-peticao', '.fieldset-cf7mls .show-apresentacao-peticao')
    syncInputWithElement('.fieldset-cf7mls .autor-peticao', '.fieldset-cf7mls .show-autor-peticao')
    syncInputWithElement('.fieldset-cf7mls .email-autor', '.fieldset-cf7mls .show-email-autor')
    syncInputWithElement('.fieldset-cf7mls .tel-autor', '.fieldset-cf7mls .show-tel-autor')
    syncInputWithElement('.fieldset-cf7mls .cidade-autor', '.fieldset-cf7mls .show-cidade-autor')
    syncInputWithElement('.fieldset-cf7mls .file-upload', '.fieldset-cf7mls .show-file-upload')

})

function syncInputWithElement(inputSelector, displaySelector) {

    const inputElement = document.querySelector(inputSelector)
    if (inputElement) {
       if(inputElement.type == 'text'){
           inputElement.addEventListener('input', function(e) {
               const displayElement = document.querySelector(displaySelector)
               if(displayElement){
                   displayElement.innerHTML = e.target.value
               }
           })
       }

       if(inputElement.tagName == 'SELECT'){
            inputElement.addEventListener('change', function(e) {
                const displayElement = document.querySelector(displaySelector)
                if(displayElement){
                    displayElement.innerHTML = e.target.value
                }
            })
       }

       if(inputElement.tagName == 'TEXTAREA'){
            inputElement.addEventListener('change', function(e) {
                const displayElement = document.querySelector(displaySelector)
                if(displayElement){
                    displayElement.innerHTML = e.target.value
                }
            })
        }

        if(inputElement.type == 'email'){
            inputElement.addEventListener('change', function(e) {
                const displayElement = document.querySelector(displaySelector)
                if(displayElement){
                    displayElement.innerHTML = e.target.value
                }
            })
        }

        if(inputElement.type == 'tel'){
            inputElement.addEventListener('change', function(e) {
                const displayElement = document.querySelector(displaySelector)
                if(displayElement){
                    displayElement.innerHTML = e.target.value
                }
            })
        }

        if(inputElement.type == 'file'){
            inputElement.addEventListener('change', function(e) {
                const displayElement = document.querySelector(displaySelector)
                if(displayElement){
                    displayElement.innerHTML = e.target.value
                }
            })
        }

    }
}

document.addEventListener('DOMContentLoaded', ()=> {
    const inputImagem = document.getElementById('inputImagem');
    const previewImagem = document.getElementById('previewImagem');
    const imagemTemporaria = document.getElementById('imagemTemporaria');
    const imagemExibida = document.getElementById('imagemExibida');

    inputImagem.addEventListener('change', function(event) {
        const arquivo = event.target.files[0];
        const reader = new FileReader();

        reader.onload = function(event) {
            const urlImagem = event.target.result;
            previewImagem.src = urlImagem;
            previewImagem.style.display = 'block';
            imagemTemporaria.value = urlImagem;
            imagemExibida.src = urlImagem;
        };

        reader.readAsDataURL(arquivo);
    });
});


