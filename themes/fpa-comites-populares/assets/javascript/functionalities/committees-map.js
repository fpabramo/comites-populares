function createFeature ({ lat, lng }, post) {
	return {
		type: 'Feature',
		geometry: {
			type: 'Point',
			coordinates: [lng, lat],
		},
		properties: {
			post,
		},
	}
}

function displayPopup (map, html, coordinates, offset = undefined) {
	const popup = new globalThis.mapboxgl.Popup({ closeOnClick: false, offset })
	popup.setLngLat(coordinates)
	popup.setHTML(html)
	popup.addTo(map)
	return popup
}

function formatPhone (phoneNumber) {
	const matches = phoneNumber.matchAll(/(\d+)/g).map(x => x[0]).toArray()
	const prefix = phoneNumber.startsWith('+') ? '' : '55'
	return prefix + matches.join('')
}

function getMapboxInstance (mapEl) {
    const uuid = mapEl.dataset.uui_id
    const jeomap = globalThis.jeomaps[uuid]
    return jeomap.map
}

function getPopupHtml (post) {
	const { area, cidade, contato, estado, pais, title } = post
	const whatsappLink = `https://wa.me/${formatPhone(contato)}`
	return `<article class="comite-popup">
		<h5>${title}</h5>
		<div class="comite-popup__location">${cidade}${estado ? ` - ${estado}` : ''}${(pais && pais !== 'Brasil') ? ` - ${pais}` : ''}</div>
		<div class="comite-popup__area">${area}</div>
		<div class="comite-popup__contact"><a href="${whatsappLink}" target="_blank">${contato}</a></div>
	</article>`
}

function loadFeatures () {
	const features = []

	document.querySelectorAll('.post-card[data-lat]').forEach((cardEl) => {
		const card = cardEl.dataset
		const lat = parseFloat(card.lat)
		const lng = parseFloat(card.lng)

		if (lat && lng) {
			const feature = createFeature({ lat, lng }, {
				title: card.title,
				contato: card.contato,
				area: card.area,
				cidade: card.cidade,
				estado: card.estado || null,
				pais: card.pais || null,
			})

			features.push(feature)
		}
	})

	return features
}

async function loadImage (map, slug, src, height, width) {
	return new Promise((resolve) => {
		const img = new globalThis.Image(width, height)
		img.onload = () => {
			map.addImage(slug, img)
			resolve(src)
		}
		img.src = src
	})
}

function loadMaps() {
	const INITIAL_CENTER = [0, 0]
	const INITIAL_ZOOM = 1
	const SPIDERIFIER_FROM_ZOOM = 10

	const features = loadFeatures()

    document.querySelectorAll('.jeomap').forEach((mapEl) => {
        const map = getMapboxInstance(mapEl)
		let lastPopup = null
		let lastZoom = INITIAL_ZOOM
		let spiderifier = null

        map.on('load', async () => {
            map.easeTo({ center: INITIAL_CENTER, zoom: INITIAL_ZOOM })

			await loadImage(map, 'comite', globalThis.committees_map_data.marker_url, 26, 26)

			map.addSource('comites', {
				type: 'geojson',
				cluster: true,
				clusterRadius: 35,
				clusterMaxZoom: 25,
				data: {
					type: 'FeatureCollection',
					features,
				},
			})

			map.addLayer({
				id: 'comites-pins',
				type: 'symbol',
				source: 'comites',
				filter: ['all', ['!has', 'point_count']],
				'layout': {
					'icon-allow-overlap': true,
					'icon-image': 'comite',
				},
			})

			map.addLayer({
				id: 'comites-clusters',
				type: 'circle',
				source: 'comites',
				filter: ['all', ['has', 'point_count']],
				paint: {
					'circle-color': '#E5172E',
					'circle-stroke-color': '#FFFFFF',
					'circle-stroke-width': 2,
					'circle-opacity': 0.9,
					'circle-radius': 12,
				},
			})

			map.addLayer({
				id: 'comites-count',
				type: 'symbol',
				source: 'comites',
				layout: {
					'text-field': '{point_count}',
					'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
					'text-size': 12,
				},
				paint: {
					'text-color': '#FFFFFF',
				},
			})

			spiderifier = new globalThis.MapboxglSpiderifier(map, {
				animate: true,
				animateSpeed: 200,
				customPin: true,
				initializeLeg (leg) {
					const html = getPopupHtml(leg.feature.post)
					leg.elements.pin.style.backgroundImage = `url("${globalThis.committees_map_data.marker_url}")`
					leg.elements.container.addEventListener('click', () => {
						lastPopup?.remove()
						lastPopup = displayPopup(map, html, leg.mapboxMarker._lngLat, MapboxglSpiderifier.popupOffsetForSpiderLeg(leg))
					})
				},
			})

			map.on('click', 'comites-pins', (event) => {
				const feature = event.features[0]
				const post = JSON.parse(feature.properties.post)
				const html = getPopupHtml(post)
				lastPopup?.remove()
				lastPopup = displayPopup(map, html, feature.geometry.coordinates)
			})

			map.on('click', 'comites-clusters', (event) => {
				const features = map.queryRenderedFeatures(event.point, { layers: ['comites-clusters'] })

				spiderifier?.unspiderfy()
				if (!features.length) {
					return
				} else if (map.getZoom() < SPIDERIFIER_FROM_ZOOM) {
					map.easeTo({ center: event.lngLat, zoom: map.getZoom() + 2 })
				} else {
					map.getSource('comites').getClusterLeaves(features[0].properties.cluster_id, 100, 0, (err, leafFeatures) => {
						if (err) {
							console.error('Error getting leaves from cluster', err)
						} else {
							const markers = leafFeatures.map((feature) => feature.properties)
							spiderifier?.spiderfy(features[0].geometry.coordinates, markers)
						}
					})
				}
			})

			map.on('zoom', (event) => {
				const currentZoom = map.getZoom()

				if (Math.abs(currentZoom - lastZoom) < 0.1) {
					if (currentZoom < lastZoom) {
						lastPopup?.remove()
						spiderifier?.unspiderfy()
					}
				}

				lastZoom = currentZoom
			})

			map.on('mousemove', (event) => {
				const features = map.queryRenderedFeatures(event.point, { layers: ['comites-pins', 'comites-clusters'] })
				map.getCanvas().style.cursor = (features.length > 0) ? 'pointer' : ''
			})

			if (window.innerWidth < 800) {
				map.dragPan.disable()
				map.scrollZoom.disable()
				map.touchPitch.disable()

				map.on('touchstart', (event) => {
					const originalEvent = event.originalEvent
					if (originalEvent && originalEvent.touches) {
						if (originalEvent.touches.length > 1) {
							originalEvent.stopImmediatePropagation()
							map.dragPan.enable()
						} else {
							map.dragPan.disable()
						}
					}
				})
			}
        })
    })
}

document.addEventListener('DOMContentLoaded', () => {
	globalThis.setTimeout(loadMaps, 0)
})
