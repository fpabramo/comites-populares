
document.addEventListener('DOMContentLoaded', function () {

    // Filtro página ferramentas de luta

    const filtroRua = document.querySelector('.filtro-rua');
    const filtroRede = document.querySelector('.filtro-rede');
    const colunaRua = document.querySelector('.rua');
    const colunaRede = document.querySelector('.rede');

    if(colunaRede){
        colunaRede.style.display = 'none';
        filtroRede.style.color = '#666';
        filtroRede.style.border = 'none';

        filtroRede.addEventListener('click', function () {
            colunaRua.style.display = 'none';
            colunaRede.style.display = 'block';
            filtroRede.style.color = '#BD2106';
            filtroRua.style.color = '#666';
            filtroRede.style.borderBottom = '8px solid #8E1521';
            filtroRua.style.border = 'none';
        });

        filtroRua.addEventListener('click', function () {
            colunaRede.style.display = 'none';
            colunaRua.style.display = 'block';
            filtroRua.style.color = '#BD2106';
            filtroRede.style.color = '#666';
            filtroRua.style.borderBottom = '8px solid #8E1521';
            filtroRede.style.border = 'none';
        });
    }

    //Modal

    const itens = document.querySelectorAll('.wp-block-post');

    if(itens){
        itens.forEach(function(iten){
            iten.addEventListener('click', function (event) {
            event.preventDefault();

            const modal = document.getElementById('myModal');
            const modalTitle = document.getElementById('modal-title');
            const modalImg = document.getElementById('modal-image');
            const downloadBtn = document.getElementById('modal-btn');
            const title = iten.querySelector('.wp-block-post-title');
            const thumb = iten.querySelector('.wp-post-image');
            const downloadLink = iten.querySelector('.hacklab-post-meta-block .meta-key-arquivo_para_download');

            modalImg.src = thumb.src
            modalTitle.innerText = title.innerText;
            downloadBtn.setAttribute('href', downloadLink.innerText);

            modal.style.display = 'block';
            })
        })
    };

    // Fechar o modal ao clicar no botão de fechar
    const closeModal = document.querySelector('#myModal .close');
    console.log(closeModal);
    closeModal.addEventListener('click', function () {
        const modal = document.getElementById('myModal');
        modal.style.display = 'none';
    });

});
