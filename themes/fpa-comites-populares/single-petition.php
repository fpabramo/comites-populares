<?php
/**
 * The template for displaying all single posts
 */

$child_id = isset($child_id) ? $child_id : null;
gt_set_post_view();

get_header();

if (empty($petition_id)) {
    $petition_id = get_the_ID();
}

$signatures_count = count_signatures($petition_id);

?>

<div class="container single" id="single">

    <div class="content" id="content">
        <?php while (have_posts()) :
            the_post();
            ?>
            <div class="post" id="post">

                <div class="post-header">

                    <div class="info">

                        <sui-sortable-handlen class="category"><?php the_category(', '); ?></sui-sortable-handlen>

                    </div>

                    <div class="title">
                        <h4> <?php the_title(); ?> </h4>
                    </div>

                    <div class="data">
                        <?php the_date(); ?>
                    </div>

                    <div class="post-share">
						<h6><?php _e('Compartilhe: ', 'comites-populares-textdomain'); ?></h6>

						<div class="social-media">
							<a class ="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank"></a>
							<a class="whatsapp" href="https://api.whatsapp.com/send?text=<?= (get_the_title() . ' - ' . get_the_permalink()); ?>" target="_blank" class="hide-for-large"></a>
							<a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()); ?>&url=<?= get_the_permalink(); ?>" target="_blank"></a>
							<a class ="telegram" href="https://t.me/share/url?url=<?= get_the_permalink(); ?>" target="_blank"><i class="fab fa-telegram-plane"></i></a>
						</div>
					</div>

                </div>

                <div class="post-content">
                    <div class="thumbnail">
                        <?php echo get_the_post_thumbnail(); ?>
                    </div>
                    <?php the_content(); ?>
                </div>

                <div class="post-footer">

                    <div class="post-footer-tags">
                        <?php echo get_html_terms(get_the_ID(), 'post_tag', true); ?>
                    </div>

                </div>

            </div>

            <!-- <?php get_template_part('template-parts/content/related-posts'); ?> -->
        <?php endwhile; ?>
    </div>

    <aside class="col-sm-3">
        <div class="petition-form">
            <span class="people-signatures"><?= $signatures_count ?></span>
            <p>Pessoas já assinaram</p>
            <h2>Assine você também</h2>
            <form action="?" method="POST" id="petition-form" data-petition-id="<?= $petition_id ?>">
                <input type="text" name="name" placeholder="Nome<?= get_post_meta(get_the_ID(), 'petition_form_nome', true) ?>" required>
                <input type="text" name="email" placeholder="E-mail" required>
                <div class="cidade">
                    <input type="text" name="cidade" placeholder="Cidade" required>
                    <select name="estado">
                        <option value="UF">UF</option>
                        <option value="AC">AC</option>
                        <option value="AL">AL</option>
                        <option value="AP">AP</option>
                        <option value="AM">AM</option>
                        <option value="BA">BA</option>
                        <option value="CE">CE</option>
                        <option value="DF">DF</option>
                        <option value="ES">ES</option>
                        <option value="GO">GO</option>
                        <option value="MA">MA</option>
                        <option value="MT">MT</option>
                        <option value="MS">MS</option>
                        <option value="MG">MG</option>
                        <option value="PA">PA</option>
                        <option value="PB">PB</option>
                        <option value="PR">PR</option>
                        <option value="PE">PE</option>
                        <option value="PI">PI</option>
                        <option value="RJ">RJ</option>
                        <option value="RN">RN</option>
                        <option value="RS">RS</option>
                        <option value="RO">RO</option>
                        <option value="RR">RR</option>
                        <option value="SC">SC</option>
                        <option value="SP">SP</option>
                        <option value="SE">SE</option>
                        <option value="TO">TO</option>
                    </select>
                </div>
                <input type="tel" name="phone" placeholder="Telefone<?= get_post_meta(get_the_ID(), 'petition_form_whatsapp', true) ?>">
                <input type="hidden" id="petition-id" name="petition_id" value="<?= $petition_id ?>">
                <input type="hidden" id="child_id" name="child_id" value="0">
                <input type="hidden" id="country" name="country" value="Brasil">

                <?php if (!empty(get_option('captcha_site_key'))) : ?>
                    <div class="g-recaptcha"
                        data-sitekey="6LdmoicqAAAAAJc69MnLleDzq_SPnd660JxVX22d"
                        data-size="invisible"
                        data-callback="onFormSubmit">
                    </div>
                <?php endif; ?>

                <button style="width: 100%" id="submit" type="submit" class="button primary mt20 block">
                    <?= get_post_meta(get_the_ID(), 'petition_submit_text', true) ?> Assinar
                </button>

                <div class="post-share">
                    <h3><?php _e('Compartilhe este abaixo assinado', 'comites-populares-textdomain'); ?></h3>

                    <div class="social-media">
                        <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank"></a>
                        <a class="whatsapp" href="https://api.whatsapp.com/send?text=<?= (get_the_title() . ' - ' . get_the_permalink()); ?>" target="_blank" class="hide-for-large"></a>
                        <a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()); ?>&url=<?= get_the_permalink(); ?>" target="_blank"><i class="fab fa-twitter-plane"></i></a>
                        <a class="telegram" href="https://t.me/share/url?url=<?= get_the_permalink(); ?>" target="_blank"><i class="fab fa-telegram-plane"></i></a>
                    </div>
                </div>

            </form>

            <div class="success-message">
                <?= get_post_meta($child_id, 'petition_terms_thank_text', true); ?>
            </div>

            <div class="repeated-signature-message">
                <?= get_post_meta($child_id, 'petition_terms_repeated_signature_text', true); ?>
            </div>
        </div>
    </aside>
</div>

<div class="modal-signature">
    <div class="content">
        <div class="entry-modal-signature">
            <h2>
                Seu apoio foi registrado com sucesso
            </h2>
            <p>
                Agradecemos seu apoio e participação nessa importante luta!
            </p>
        </div>
        <div class="close-modal">
            <button class="button-close">Fechar</button>
        </div>
    </div>
</div>

<?php get_footer(); ?>
