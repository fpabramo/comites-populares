<?php get_header(); ?>
<div class="error-404">

    <div class="title-404">
        <h1>   
            <span class="error"><?php _e('erro', 'base-textdomain') ?></span>
            <span class="num">404</span>
        </h1>
    </div>

        <p class="excerpt-404"><?php _e('Ops, página não encontrada. A página que você estava procurando pode ter mudado ou não existe mais.', 'base-textdomain') ?></p>
        <a href="<?= home_url() ?>" class="button"> <span><?php _e('Voltar para a página inicial', 'base-textdomain') ?></span> </a>
    </div>

<?php get_footer(); ?>
