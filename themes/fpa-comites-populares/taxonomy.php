<?php
get_header();

$term = get_queried_object();

?>

<div class="index-wrapper">
    <div class="container">
        <div class="row">
            <h2 class="termo-nome"><?php echo $term->name;?></h2>
            <main class="col-md-12">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'template-parts/content/post-midias' ); ?>
                <?php endwhile; ?>

                <div id="myModal" class="modal">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <h5 id="modal-title"></h5>
                        <img id="modal-image" src="" alt="Imagem para download">
                        <a id="modal-btn" download>Baixar</a>
                        <p>Compartilhe</p>
                        <?php share_menu() ?>
                    </div>
                </div>

                <?php get_template_part( 'template-parts/content/pagination' ); ?>
            </main>

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.index-wrapper -->

<?php get_footer();
