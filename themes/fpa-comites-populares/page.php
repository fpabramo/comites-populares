<?php
get_header();
?>

<div class="index-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 content">
                <?php
                if (is_page('trabalho-de-base')) :
                    echo get_layout_header('trabalho-de-base');
                else :?>
                    <h2>
                        <?php the_title() ?>
                    </h2>

                    <div class="post-share">
                        <h6><?php _e('Compartilhe: ', 'comites-populares-textdomain'); ?></h6>

                        <div class="social-media">
                            <a class ="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank"></a>
                            <a class="whatsapp" href="https://api.whatsapp.com/send?text=<?= (get_the_title() . ' - ' . get_the_permalink()); ?>" target="_blank" class="hide-for-large"><i class="fab fa-whatsapp"></i></a>
                            <!-- <a class="instagram" href="mailto:?subject=<?php //the_title();?>&amp;body=<?php //_e( 'Visite a página', 'reconexao-periferias-textdomain' );?> <?php //the_title();?> <?php //_e( 'no site', 'reconexao-periferias-textdomain' );?> <?php //echo bloginfo( 'name' );?>" target="_blank"><i class="fab fa-email"></i></a> -->
                            <a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink(); ?>" target="_blank"><i class="fab fa-twitter-plane"></i></a>
                            <a class ="telegram" href="https://telegram.me/share/url?url=<?= get_the_title() . ' - ' . get_the_permalink(); ?>" target="_blank"><i class="fab fa-telegram-plane"></i></a>
                        </div>
                    </div>

                <?php endif; ?>

                <?php
                    // Modal página Ferramentas de luta
                    if (is_page('ferramentas-de-luta') || is_page('loja')) {
                        ?>
                        <div id="myModal" class="modal">
                            <div class="modal-content">
                                <span class="close">&times;</span>
                                <h5 id="modal-title"></h5>
                                <img id="modal-image"  width="330px" height="330px" src="" alt="Imagem para download">
                                <a id="modal-btn" download>Baixar</a>
                                <p>Compartilhe</p>
                                <?php share_menu() ?>
                            </div>
                        </div>
                        <?php
                    }
?>


                <?php the_content() ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();
