<?php

    class shareItem{

        function __construct($name, $params = []){
            global $post;

            $this->name = $name;
            $this->post = $post;
            $this->is_svg = $params['svg'];
            $this->link = $params['link'];
        }

        public function name(){
            return $this->name;
        }

        public function icon(){
            $icons_img_dir = get_template_directory() . '/assets/images/social-networks/';
            $icons_img_uri = get_template_directory_uri() . '/assets/images/social-networks/';

            if ($this->is_svg)
                return  file_get_contents($icons_img_dir . $this->name . '.svg');
            else
                return '<img src="' . $icons_img_uri . $this->name . '.png" alt="' . $this->name . '" height="64" width="64">';
        }

        public function url(){
        return $this->link . get_the_permalink($this->post->ID);
        }

        public function media_name(){
            return ($this->media()) ? $this->post->post_name : false;
        }


        public function get_download_item(){
            $downlodable = false;

            if(get_post_meta($this->post->ID, 'arquivo_download'))
                $downlodable = get_post_meta($this->post->ID, 'arquivo_download')[0]["guid"];

            return $downlodable;
        }

        public function media(){
            if($this->get_img())
                return $this->get_img();

            if($this->get_download_item())
                return $this->get_download_item();

            return '';
        }

        public function get_img(){
            $first_img = false;
            ob_start();
            ob_end_clean();
            $output = preg_match_all('/<img.+?src=[\'"]([^\'"]+)[\'"].*?>/i', $this->post->post_content, $matches);

            if(isset($matches[1][0]))
                $first_img = str_replace(site_url(), "",  $matches[1][0]);

            return $first_img;
        }

        private $post,
                $name,
                $is_svg,
                $link;
    }
