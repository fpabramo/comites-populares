<?php

namespace JeoGeocoding;

class Geocoder {

	protected static ?self $_instance = null;
	private string $api_key;

	protected function __construct() {
		$this->api_key = get_option('comites_gmaps_key', '');
	}

	public static function instance(): self {
		if (is_null(self::$_instance)) {
			$class = get_called_class();
			self::$_instance = new $class();
		}

		return self::$_instance;
	}

	public function fetch (string $address) {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . $this->api_key . '&address=' . urlencode($address);

		$response = wp_remote_get($url, []);

		if (is_wp_error($response)) {
			return false;
		}

		$json = json_decode($response['body']);

		$best_match = $json->results[0];

		return [
			'address' => $best_match->formatted_address,
			'lat' => $best_match->geometry->location->lat,
			'lng' => $best_match->geometry->location->lng,
		];
	}

	private function get_meta ($post, $metaKey, $default = null) {
		$metaValue = get_post_meta($post->ID, $metaKey, true);

		if (empty($metaValue)) {
			return $default;
		}

		if (is_numeric($metaValue)) {
			return floatval($metaValue);
		}

		return $metaValue;
	}

	public static function register_post_type () {
		register_post_type('comites_geocode', [
			'labels' => [],
			'hierarchical' => false,
			'supports' => ['custom-fields'],
			'public' => false,
			'show_in_menu' => false,
			'show_in_rest' => false,
			'exclude_from_search' => true,
		]);
	}

	public function search (string $address) {
		$query = new \WP_Query([
			'post_type' => 'comites_geocode',
			'meta_query' => [
				[ 'key' => 'input', 'value' => $address ],
			],
		]);

		if ($query->have_posts()) {
			$post = $query->post;

			return [
				'address' => $this->get_meta($post, 'address', $address),
				'lat' => $this->get_meta($post, 'lat', 0.0),
				'lng' => $this->get_meta($post, 'lng', 0.0),
			];
		}

		$api_result = $this->fetch($address);

		if ($api_result) {
			wp_insert_post([
				'post_type' => 'comites_geocode',
				'post_status' => 'publish',
				'meta_input' => [
					'address' => $api_result['address'],
					'input' => $address,
					'lat' => $api_result['lat'],
					'lng' => $api_result['lng'],
				],
			]);
		}

		return $api_result;
	}
}

add_action('init', [Geocoder::class, 'register_post_type']);

function geolocate_comites_posts ($meta_id, $post_id, $meta_key, $meta_value) {
	$address_keys = ['cidade', 'estado', 'pais'];

	if (get_post_type($post_id) !== 'comite' || !in_array($meta_key, $address_keys)) {
		return;
	}

	if (empty(get_post_meta($post_id, 'cidade', true)) || (empty(get_post_meta($post_id, 'estado', true)) && empty(get_post_meta($post_id, 'pais', true)))) {
		return;
	}

	$post_meta = get_post_meta($post_id);

	$address_parts = [];

	foreach ($address_keys as $part) {
		if (!empty($post_meta[$part]) && !empty($post_meta[$part][0]) && $post_meta[$part][0] !== 'ND') {
			$address_parts[] = trim($post_meta[$part][0]);
		} else if ($part === 'pais') {
			$address_parts[] = 'Brasil';
		}
	}

	$address = implode(', ', $address_parts);

	$geocode = Geocoder::instance()->search($address);

	update_post_meta($post_id, 'lat', $geocode['lat']);
	update_post_meta($post_id, 'lng', $geocode['lng']);
}
add_action('added_post_meta', 'JeoGeocoding\geolocate_comites_posts', 10, 4);
add_action('updated_post_meta', 'JeoGeocoding\geolocate_comites_posts', 10, 4);
