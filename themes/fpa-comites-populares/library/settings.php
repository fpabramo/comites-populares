<?php

namespace JeoGeocoding;

class SettingsPage {

	private static string $PAGE = 'comites-populares';

	public static function __blank () {
		return '';
	}

	public static function __identity ($value) {
		return $value;
	}

	public static function register_fields () {
		$GEOLOCATION_SECTION = 'geolocation';

		$GMAPS_KEY_FIELD = 'comites_gmaps_key';

		add_settings_section(
			$GEOLOCATION_SECTION,
			'Geolocalização',
			null,
			self::$PAGE,
		);

		register_setting(
			self::$PAGE,
			$GMAPS_KEY_FIELD,
			[
				'sanitize_callback' => [self::class, '__identity'],
			],
		);

		add_settings_field(
			$GMAPS_KEY_FIELD,
			'Chave de API do Google Maps',
			[self::class, 'render_text_field'],
			self::$PAGE,
			$GEOLOCATION_SECTION,
			[
				'label_for' => $GMAPS_KEY_FIELD,
				'name' => $GMAPS_KEY_FIELD,
			],
		);
	}

	public static function register_page ($args) {
		add_menu_page(
			'Comitês Populares',
			'Comitês',
			'manage_options',
			self::$PAGE,
			[self::class, '__blank'],
			get_theme_file_uri('/assets/images/menu-icon.svg'),
			90
		);

		add_submenu_page(
			self::$PAGE,
			'Comitês Populares',
			'Comitês',
			'manage_options',
			self::$PAGE,
			[self::class, 'render_page'],
		);
	}

	public static function render_page () {
	?>
		<div class="wrap">
			<h1><?= esc_html(get_admin_page_title()) ?></h1>
			<form action="options.php" method="post">
			<?php
				settings_fields(self::$PAGE);
				do_settings_sections(self::$PAGE);
				submit_button();
			?>
			</form>
		</div>
	<?php
	}

	public static function render_text_field ($args) {
		$option = get_option($args['name'], '');
	?>
		<input
			id="<?= esc_attr($args['label_for']) ?>"
			name="<?= esc_attr($args['name']) ?>"
			type="text"
			value="<?= esc_attr($option) ?>"
		>
	<?php
	}
}

add_action('admin_init', [SettingsPage::class, 'register_fields']);
add_action('admin_menu', [SettingsPage::class, 'register_page']);
