<?php

function comites_redirects() {

	global $wp_query;

	/**
	 * Redirect URL /collections to /acervo
	 */
	if ( is_post_type_archive( 'ferramenta_de_luta' ) ) {
		wp_redirect( home_url( 'ferramentas-de-luta' ) );
		exit;
	}

}

add_action( 'template_redirect', 'comites_redirects' );
