<?php


$post_id  = get_the_ID();
$upload = get_post_meta($post_id, 'arquivo_para_download', true); ?>



<article id="post-ID-<?php the_ID(); ?>" class="post wp-block-post">
    <div class="post-card">
        <div class="post-card--thumb">
            <a href="<?php the_permalink(); ?>">
                <div class="aspect-ratio">
                    <?php if ( has_post_thumbnail() ) : ?>
                        <?php the_post_thumbnail( 'medium_large', array( 'class' => 'download-midia wp-post-image' ) ); ?>
                    <?php else : ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-image.png" alt="<?php the_title(); ?>">
                    <?php endif; ?>
                </div><!-- /.aspect-ratio -->
            </a>
        </div><!-- /.post-card--thumb -->

        <div class="post-card--content">
            <div class="entry-meta"><?php echo get_html_terms( get_the_ID(), 'category', true ); ?></div>
            <a href="<?php the_permalink(); ?>"><h5 class="entry-title wp-block-post-title download-midia"><?php the_title(); ?></h5></a>
            <div class="entry-date"><?php echo get_the_date(); ?></div>
            <div class="entry-excerpt">
                <?php echo custom_excerpt( ( str_word_count( get_the_title() ) <= 10 ) ? 15 : 20 ); ?>
            </div>
        </div><!-- /.post-card--content -->
    </div><!-- /.post-card -->
    <?php if ($upload) : ?>
        <div class="hacklab-post-meta-block">
        <span class="meta-key-arquivo_para_download"><?php echo esc_url(wp_get_attachment_url($upload)); ?></span>
        <span class="meta-download" id="upload-meta-<?php the_ID(); ?>" data-upload-url="<?php echo esc_url(wp_get_attachment_url($upload)); ?>"></span>
        </div>
    <?php endif; ?>
</article><!-- /.post -->
