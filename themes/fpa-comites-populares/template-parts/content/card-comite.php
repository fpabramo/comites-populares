<?php

$post_id = get_the_ID();
$area_de_atuacao = get_post_meta($post_id, 'area_de_atuacao', true);
$cidade = get_post_meta($post_id, 'cidade', true);
$estado = get_post_meta($post_id, 'estado', true);
$pais = get_post_meta($post_id, 'pais', true);
$contato = get_post_meta($post_id, 'contato', true);
$latitude = get_post_meta($post_id, 'lat', true);
$longitude = get_post_meta($post_id, 'lng', true);
$title = get_the_title();

?>
<article id="post-ID-<?php the_ID(); ?>" class="post">
    <div class="post-card"
    data-title="<?= $title ?>"
    data-cidade="<?= $cidade ?>"
    data-estado="<?= $estado ?>"
    data-pais="<?= $pais ?>"
    data-area="<?= $area_de_atuacao ?>"
    data-contato="<?= $contato ?>"
	data-lat="<?= $latitude ?>"
	data-lng="<?= $longitude ?>"
    >

        <div class="post-card--content">
            <a href="<?php the_permalink(); ?>"><h5 class="entry-title"><?php the_title(); ?></h5></a>
            <div class="meta-comite">
				<div class="location">
					<div class="cidade"> <?= $cidade ?> </div>
					<div class="estado">
						<?php if(!empty($estado)) {
							echo  ' - ' . $estado;
						} ?>
					</div>
					<div class="pais">
						<?php if(!empty($pais) && $pais != 'Brasil') {
							echo  ' - ' . $pais;
						} ?>
					</div>
				</div>
                <div class="area-atuacao"><?= $area_de_atuacao ?></div>
            </div>
        </div><!-- /.post-card--content -->
    </div><!-- /.post-card -->
</article><!-- /.post -->
