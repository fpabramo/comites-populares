<?php

/**
 * Get the search string
 */
$search_query = get_search_query( false );

if ( ! empty( $search_query ) ) {
    $title = 'Resultados da busca para <span class="highlighted">'.'"' . esc_attr( $search_query  ) . '</span>'.'" ';
} else {
    $title = 'Pesquisar';
} ?>

<header class="c-title title-search">

    <div class="container">
        <h1 class="entry-title">
            <?php echo apply_filters( 'the_title' , $title ); ?>
        </h1>
        <div class="search-count">
            <p>
                <?php
                global $wp_query;

                $not_singular = $wp_query->found_posts > 1 ? 'resultados foram encotrados' : 'resultado foi encontrado'; 
                // if found posts is greater than one echo results(plural) else echo result (singular)
                echo $wp_query->found_posts . " $not_singular "; ?>
            </p>
    </div>
    </div>

    
</header><!-- /.c-title.title-search -->