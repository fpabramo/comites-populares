<div class="share-links">
    <div class="share-links--content">
        <!-- <span class="hidden-desktop"><?php _e( 'Compartilhe', 'base-textdomain' ) ?></span> -->
        <span class="share hidden-mobile"></span>
        <a href="https://api.whatsapp.com/send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" class="whatsapp hidden-mobile" target="_blank"></a>
        <a href="whatsapp://send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" target="_blank" class="whatsapp hidden-desktop"></a>
        <a href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" class="twitter" target="_blank"></a>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" class="facebook" target="_blank"></a>
        <a href="https://telegram.me/share/url?url=<?= get_the_title().' - '.get_the_permalink() ?>" class="telegram" target="_blank"></a>
        <a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php _e( 'Visite a página', 'reconexao-periferias-textdomain' ); ?> <?php the_title(); ?> <?php _e( 'no site', 'reconexao-periferias-textdomain' ); ?> <?php echo bloginfo( 'name' ); ?>" title="Share by Email" class="email"></a>

    </div>
</div>
