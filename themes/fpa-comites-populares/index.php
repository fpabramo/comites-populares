<?php
get_header();
?>

<div class="index-wrapper">
    <div class="container">
        <div class="row">
            <?php if ( is_home() ) : ?>
                <?php echo get_layout_header( 'noticias' ); ?>

                <main class="col-md-9">
                    <div class="content">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php get_template_part( 'template-parts/content/post' ); ?>
                        <?php endwhile; ?>
                    </div>
                    

                    <?php get_template_part( 'template-parts/content/pagination' ); ?>
                </main>

            <?php else : ?>
                <div class="col-md-12">
                    <?php the_content() ?>
                </div>
            <?php endif; ?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.index-wrapper -->

<?php get_footer();
