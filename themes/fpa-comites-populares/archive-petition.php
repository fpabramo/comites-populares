<?php
get_header();
?>

<div class="index-wrapper">
    <div class="container">
        <div class="row">

        <?php if ($post_type === 'petition') :
            echo get_layout_header('petitions');
        
        else :
            $categories = get_the_category();
            $tag = get_the_tags();

            if (is_category() && !empty($categories)) : ?>
                <div class="header-and-footer-archive position-header">
                    <h2><?php echo esc_html($categories[0]->name);?></h2>
                </div>
            <?php
            elseif (is_tag() && !empty($tag)) : ?>
                <div class="header-and-footer-archive position-header">
                    <h2><?php echo esc_html($tag[0]->name);?></h2>
                </div>
            <?php
            endif;
        endif; ?>

<main class="col-md-12">
    
    <div class="content">
        <?php while (have_posts()) : the_post();
                    ?>
                    <?php get_template_part('template-parts/content/post'); ?>
                    <?php endwhile; ?>
                </div>
                
                <div class="sidebar-archive-petition">
                    <?php dynamic_sidebar('sidebar_petition') ?>
                </div>
                    <?php get_template_part('template-parts/content/pagination'); ?>
                </main>

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.index-wrapper -->

<?php get_footer();
