</div>
<?php wp_reset_postdata() ?>

<footer class="main-footer">
    <div class="container">
        
       

        <div class="row">
            <div class="col-md-12">

                <div class="row footer-menus">
                    <!-- <div class="social-networks">
                        <?php the_social_networks_menu() ?>
                    </div> -->
                </div>
                
            </div>

            <div class="col-md-12 copyright-area">

            <div class="footer-copyright-area">
            <div class="post-footer-share">
						<div class="social-media">
							<a class ="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank"></a>
							<a class="whatsapp" href=" href="https://api.whatsapp.com/send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>"" target="_blank" class="hide-for-large"></a>
							<a class="instagram" href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php _e( 'Visite a página', 'comites-populares-textdomain' ); ?> <?php the_title(); ?> <?php _e( 'no site', 'comites-populares-textdomain' ); ?> <?php echo bloginfo( 'name' ); ?>" target="_blank"></a>
							<a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" target="_blank"></a>
							<a class ="telegram" href="https://telegram.me/share/url?url=<?= get_the_title().' - '.get_the_permalink() ?>" target="_blank"></a>
						</div>
					</div>
                <?php dynamic_sidebar('footer_copyright_area') ?>
            </div>
            </div>
            <!-- <div class="col-md-12 addicional-info">

                <?php 
                    $copyright_option = get_theme_mod( 'footer_copyright_text', '' );
                    $show_name_and_year = checked( 1, get_theme_mod('footer_show_year_and_name'), false );
                    if(!empty($copyright_option)): ?>

                    <div class="copyright-info">
                        <?= $copyright_option ?>
                        <?php 
                        if($show_name_and_year): 
                            echo date("Y") . " "; 
                            echo bloginfo("name");
                        endif;
                        ?>
                    </div>

                <?php
                    endif;
                ?>

            </div> -->


        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>