<?php
get_header();
$maps = get_posts([ 'post_type' => 'map', 'numberposts' => 1 ]);
?>

<div class="index-wrapper">
    <div class="container">
        <div class="row">

        <?php if ($post_type === 'comite') :
            echo get_layout_header('comite');

        else :
            $categories = get_the_category();
            $tag = get_the_tags();

            if (is_category() && !empty($categories)) : ?>
                <div class="header-and-footer-archive position-header">
                    <h2><?php echo esc_html($categories[0]->name);?></h2>
                </div>
            <?php
            elseif (is_tag() && !empty($tag)) : ?>
                <div class="header-and-footer-archive position-header">
                    <h2><?php echo esc_html($tag[0]->name);?></h2>
                </div>
            <?php
            endif;
        endif; ?>

<main class="col-md-12">

    <div class="content">
        <div class="cards-comite">
            <form role="search" method="get" class="search-form" >
                <label class="input-resultado-pesquisa">
                    <span class="screen-reader-text"><?php echo _x('Search for:', 'label') ?></span>
                    <input type="search" class="search-field" placeholder="<?php echo esc_attr_x('Pesquise por comitês', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="search" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
                    <input type="submit" class="search-submit" value="<?php echo esc_attr_x('Search', 'submit button') ?>" />
                </label>
            </form>
            <div class="height-content-comite">
                <?php while (have_posts()) : the_post();
                     ?>
                    <?php get_template_part('template-parts/content/card-comite'); ?>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="mapa">
			<?php if (!empty($maps)): ?>
				<div class="jeomap map_id_<?= $maps[0]->ID ?>"></div>
			<?php endif; ?>
        </div>
    </div>

<!-- <div class="sidebar-archive-petition">
    <?php dynamic_sidebar('sidebar_petition') ?>
</div> -->
    <?php get_template_part('template-parts/content/pagination'); ?>
</main>

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.index-wrapper -->

<?php get_footer();
